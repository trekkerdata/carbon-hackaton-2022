import requests
import time
from datetime import datetime
from requests.structures import CaseInsensitiveDict

# set parameters
url = "https://api.trekkerdata.nl/v1/download/21/1647327712-1e05ea75-b1f7-49a7-b847-e76931438d88"

headers = CaseInsensitiveDict()
headers["Accept"] = "application/json"
headers["Authorization"] = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkFyTW8wYXNrOUcyYWRUNDhlamxBUyJ9.eyJpc3MiOiJodHRwczovL2xvZ2luLnRyZWtrZXJkYXRhLm5sLyIsInN1YiI6ImF1dGgwfDYyMzdhYmIxNzcwYzgxMDA3MjhhZmE0YyIsImF1ZCI6WyJodHRwczovL2FwaS50cmVra2VyZGF0YS5ubCIsImh0dHBzOi8vZGV2LTNvY2R6N3V4LmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE2NDc4NzEzMTAsImV4cCI6MTY0Nzk1NzcxMCwiYXpwIjoiajk4ZlRhUVhrTFdsa21mWnRoNWlVNVhYcUc2N3FXOWEiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwicGVybWlzc2lvbnMiOltdfQ.X7FoT_ag_eZ7RI2dDknOmL4YkN6JD0yae-pdDMw4C1W2iD_rF1KJyzCBYl5Ud9vGdI3TJVFsrENrhPgqDLgEy21mwFv81Eam0du6ZI5rctblyDc_p7fH3SJ7c27QGSNQAxH6xX5thhCqmmtj5uMltm792yc7BAuHAYLKzy7JUHGv262SDnXfq62n9h7oLlvrDrT7O3EgB0FnDnqhuGBE3o6oap3wkkcXuO_5wp_KsbtH0sDiv-toXB9czI4sBTRR4Fl3JUigjcI7Qn6uB9-bofLGdKK0yjs8jc-IOqydh8kHg-0SvTZtpGRmmQ6oBU7EOHHa7Arwf4OeGtGh888fOQ"


# download method
def download(file):

  ts = datetime.now()
  
  print(ts.strftime("%H:%M:%S") + " Downloading " + file)

  resp = requests.get(url, headers=headers)
  f = open("data/"+file, "w+")
  # f.write(resp)
  f.close()

  ts = datetime.now()
  
  print(ts.strftime("%H:%M:%S") + " Completed " + file)



# read sessions file
sessions = open("all_sessions.csv")


while True:

  line = (sessions.readline()).rstrip()

  if line != "":

    splitted = line.split(",",4)
    
    download(splitted[3])

  else:
    break

print("Completed!")
