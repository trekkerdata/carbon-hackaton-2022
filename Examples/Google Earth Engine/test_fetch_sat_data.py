import fetch_sat_data
from fetch_sat_data import SatDataFetcher
from field_locator import FieldLocator
import ee
import geemap
import shapely
import inspect
from pathlib import Path


import pyproj
from shapely.ops import transform


def get_roi_wgs84(roi_shp, src_crs):

    dest_crs = 'epsg:4326'  # WGS-84

    if src_crs != dest_crs:
        project = pyproj.Transformer.from_proj(pyproj.Proj(init=src_crs),  # source coordinate system
                                               pyproj.Proj(init=dest_crs))  # destination coordinate system

        # g1 is a shapley Polygon
        roi_shp_epsg4326 = transform(project.transform, roi_shp)  # apply projection
    else:
        roi_shp_epsg4326 = roi_shp

    roi_coords = get_coord_list(roi_shp_epsg4326)

    roi = get_roi_from_coords(roi_coords, proj=dest_crs)
    return roi


def get_roi_from_coords(coordinates_list, proj):
    # Create a polygon of this coordinates
    polygon = ee.Geometry.Polygon(coordinates_list, proj=proj)
    polygon_roi = ee.Feature(polygon)

    # Get the geometry
    roi = polygon_roi.geometry()

    return roi


def get_coord_list(feature):
    if isinstance(feature, shapely.geometry.polygon.Polygon):
        coordinates_list = _fetch_coord_list_polygon(feature)
    elif isinstance(feature, shapely.geometry.multipolygon.MultiPolygon):
        coordinates_list = _fetch_coord_list_multipolygon(feature)
    else:
        raise Exception("Unsupported feature type: {info:}".format(info=inspect.getmembers(feature)))

    return coordinates_list


def _fetch_coord_list_polygon(feature):
    x, y = feature.exterior.coords.xy
    coordinates_list = [list(zip(x, y))]
    return coordinates_list


def _fetch_coord_list_multipolygon(feature):
    polygons = list(feature)
    coordinates_list = []
    for polygon in polygons:
        coordinates_list += _fetch_coord_list_polygon(polygon)

    return coordinates_list


def main():
    #  check some bugs (saving geojson) + tuning (e.g. cloud filtering, median/mean)
    #  check if tif with different R G B bands can be combined in 1 tif that then also visually looks good

    # First time you using, you should authenticate (so comment this out, after you have run this already once)
    # Make sure you have your gmail account linked to https://earthengine.google.com/
    #  -> remark only non-commercial use, you can always fill in the url of a university, as a reference page for your
    #     associated research

    # ee.Authenticate()

    ee.Initialize()

    # Provide a file with one or multiple field polygons and load them all, optionally based on selection using a
    # a certain attribute

    # use1: no filtering on certain attributes --> load all polygons from the file
    #fieldlocator = FieldLocator(source='./fields/test1_prjEPSG28991.shp')

    # use2: filter on certain attribute --> e.g. use attribute 'field_name', assuming it exists
    field_names = ['field_flevo_1', 'flevo4']
    #fieldlocator = FieldLocator(source='./fields/test1_prjEPSG28991.shp', attribute_name='field_name', attribute_list=field_names)

    # use3: filter on certain attribute --> e.g. 'owner', assuming it exists
    owners = ['John Doe']
    fieldlocator = FieldLocator(source='./fields/test1_prjEPSG28991.shp', attribute_name='owner', attribute_list=owners)

    # Plot them, just to check they make sense
    fieldlocator.plot_field_geoms()

    # Now fetch the geometries, which will be used then to extract the right satellite data
    geoms = fieldlocator.get_field_geoms()
    # Fetch also a certain attribute, that will be used to to make directories on disk. So assumption is that this
    # attribute is unique (e.g. field_name). This assumption may not be valid... so check carefully.
    field_names = fieldlocator.field_gdfs['field_name']

    # Define if you want 'NDVI' or 'RGB' data (other indices could be defined in fetch_sata_data, similar to get_ndvi)
    CHANNELS = 'RGB'

    # Indicate the start and and date you want to download data
    # Remark: for now hardcoded, that it will create a dataset for every month (and thus this means that multiple
    #  satellite observations are "reduced" to a single value (via median --> you can also change to mean e.g., is in fetch_sat_data)
    start_year = 2020
    end_year = 2021
    start_month = 1
    end_month = 12

    # For every separate field, load the satellite data
    for field_geom, field_name in zip(geoms, field_names):
        src_crs = fieldlocator.field_gdfs.crs
        roi = get_roi_wgs84(roi_shp=field_geom, src_crs=src_crs)

        # Loop over year, month
        for yyyy in range(start_year, end_year + 1):
            if yyyy == end_year:
                _end_month = end_month
            else:
                _end_month = 12

            for mm in range(start_month, _end_month + 1):
                dates = fetch_sat_data.get_date_from_yearmonth(yyyy, mm)
                init, ends = dates[0], dates[1]

                out_dir = './sat_imgs/' + field_name + '/' + CHANNELS + '/'
                out_file = out_dir + init + '_' + ends
                Path(out_dir).mkdir(parents=True, exist_ok=True)

                try:
                    # Create an image collection
                    sat_data_fetcher = SatDataFetcher(roi=roi, start_date=init, end_date=ends, channels=CHANNELS)

                    sat_data_fetcher.export_tif(filename=out_file)

                except Exception as e:
                    print(e)


if __name__ == "__main__":
    main()
