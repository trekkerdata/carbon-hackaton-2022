# Using google earth engine and python module geemap
Author: Arno Leenknegt (arno@trekkerdata.nl)

## Requirements

You need to link your gmail to google earth engine.

> python3 modules
> - ee,  geemap
> - rasterio, shapely, geopandas
> - geopandas
> - matplotlib
>

You can set up the environment using conda as follows:

> - conda create -n satellite_env python=3.7
>
> - conda activate satellite_env
>
> - conda config --add channels conda-forge
> - conda config --set channel_priority strict
>
> - conda install rasterio  (with python=3.7.11, this installs rasterio=1.2.10)
> - conda search rasterio --channel conda-forge
>
> - conda install -c conda-forge matplotlib
>
> - conda install pandas
>
> - conda install geopandas
>
> - conda install geemap -c conda-forge
> - conda install mamba -c conda-forge
> - mamba install geemap xarray_leaflet -c conda-forge

## Example use
### Region of interest
You need one or multiple polygons (e.g. in .shp or .geojson format) defining the region of interests.
The coordinate system can be WGS84, but also any projected coordinate system. But as google earth engine requires WGS84, this has to be reprojected. This is done automatically in the script `test_fetch_sat_data.py` via `get_roi_wgs84` .

The polygons can be loaded via the field_locator module, remark that you can optionally select certain polygons, based upon an `attribute_name` and accompanying `attribute_list`.

### Downloading data
If you run test_fetch_sat_data, based on the provided geo-file, it will download data from a certain `start_year`/`start_month` to a certain `end_year`/`end_month`.
The code will generate and export data into a .tif file, for every month (meaning that per pixel values are reduced via "median", for multiple observations within 1 month.). If you want the reducer to be different, or the cloud-pixel filter different, please check `fetch_sat_data.py::fetch_img_collection`.

You can choose to download the "RGB" channels, or "NDVI", see parameter `channels`.

### Understanding the data
`combine_sat_channels.py`shows how to combine R, G and B .tifs into a single RGB tif. Remark that the brightness has to be adjusted manually (see BRIGHTNESS_FACTOR), since the R G B channels are not exactly between 0-1 or 0-255. Also when you open .tif file, it may just look gray (but that depends on your image view), please open as well with QGIS (who does automatic rescaling).


