# import google earth engine
import ee
# import python module on top of google earth engine
import geemap
# import time module, to fetch dates from week numbers
import time
import datetime


class SatDataFetcher:
    def __init__(self, roi, start_date, end_date, channels='RGB'):
        self.roi = roi
        self.img_collection = self.fetch_img_collection(roi, start_date, end_date, channels=channels)

    def export_tif(self, filename):

        img_collection = self.img_collection

        satImg = ee.Image(img_collection)

        # Final region clipping, and then export
        image = satImg.clip(self.roi).unmask()
        geemap.ee_export_image(image, filename=filename + '.tif', scale=10, region=self.roi, file_per_band=True)

    def export_geojson(self, filename):
        # Can't get this to work now... used to work

        img_collection = self.img_collection
        satImg = ee.Image(img_collection) #.select('NDVI_median')

        # Make a feature collection from the ROI (will be used to extract the right points)
        in_fc = ee.FeatureCollection([self.roi])

        geemap.extract_values_to_points(in_fc=in_fc, image=satImg, out_fc=filename + '.geojson', scale=10)

        return None


    def get_ndvi(self, image):
        """Here we apply the NDVI calculation"""

        ndvi = image.normalizedDifference(['Nir', 'Red']).rename('NDVI')
        return image.addBands(ndvi)

    def maskS2clouds(self, image):
        qa = image.select('QA60')
        cloud_bitmask = 1 << 10
        cirrus_bitmask = 1 << 11
        mask = qa.bitwiseAnd(cloud_bitmask).eq(0).And(
            qa.bitwiseAnd(cirrus_bitmask).eq(0))
        return image.updateMask(mask).divide(10000)

    def fetch_img_collection(self, roi, start_date='2020-09-01', end_date='2020-10-01', channels='RGB'):
        CLOUDY_PIXEL_PERCENTAGE = 50

        # Defaults:
        # > with mask
        # > CLOUDY_PIXEL_PERCENTAGE: 50
        # > reducer: median

        if channels is 'RGB':
            img_collection = ee.ImageCollection("COPERNICUS/S2") \
                .select(['B2', 'B3', 'B4', 'B8', 'QA60'], ['Blue', 'Green', 'Red', 'Nir', 'QA60']) \
                .filterDate(start_date, end_date) \
                .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', CLOUDY_PIXEL_PERCENTAGE)) \
                .map(self.maskS2clouds) \
                .reduce(ee.Reducer.median()) \
                .clip(roi)
            return img_collection

        elif channels is 'NDVI':
            img_collection = ee.ImageCollection("COPERNICUS/S2") \
                .select(['B3', 'B4', 'B8', 'QA60'], ['Green', 'Red', 'Nir', 'QA60']) \
                .filterDate(start_date, end_date) \
                .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', CLOUDY_PIXEL_PERCENTAGE)) \
                .map(self.maskS2clouds) \
                .map(self.get_ndvi) \
                .reduce(ee.Reducer.median()) \
                .clip(roi)
            return img_collection
        # palette =  cm.palettes.ndvi
        # Map.addLayer(S2col, {'min': -0.5, 'max': 0.9, 'palette': palette}, 'S2col')


def get_date_from_yearmonth(y, m):
    startYEAR = y
    startMONTH = m
    startDAY_MONTH = 1

    if m == 12:
        endYEAR = startYEAR + 1
        endMONTH = 1
    else:
        endYEAR = startYEAR
        endMONTH = startMONTH + 1
    endDAY_MONTH = 1

    dates = []

    startdate = time.asctime(time.strptime('%d %d %d' % (startYEAR, startMONTH, startDAY_MONTH),
                                           '%Y %m %d'))  # format %Y (year) %m  (month) %d (day of the month)
    startdate = datetime.datetime.strptime(startdate, '%a %b %d %H:%M:%S %Y')
    dates.append(startdate.strftime('%Y-%m-%d'))

    enddate = time.asctime(time.strptime('%d %d %d' % (endYEAR, endMONTH, endDAY_MONTH),
                                         '%Y %m %d'))  # format %Y (year) %m  (month) %d (day of the month)
    enddate = datetime.datetime.strptime(enddate, '%a %b %d %H:%M:%S %Y')
    dates.append(enddate.strftime('%Y-%m-%d'))

    return dates  # list with start date and end data (start: inclusive, end: exlusive)