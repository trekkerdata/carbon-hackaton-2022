import geopandas as gpd
from pathlib import Path


class PolygonLoader:
    def __init__(self, geofile):
        # Full file path (abs/rel) to geo file
        self.geofile = geofile

        # load the shapes/geoms from the file
        self.shapes_gdf, self.crs = self._load_shapes_gpd()

    def get_all(self):
        """ Public method, to return all the outline polygons in a list """
        return self.get_on_spec(attribute_name=None, attribute=None)

    def get_on_idx(self, index):
        """ Public method, to return the a single outline polygon (still in list), based on an index """
        shapes = self.get_all()
        if 0 <= index < len(shapes):
            return [shapes[index]]
        else:
            raise OverflowError("Index " + index + " out of range for outlines-list of length: "
                                + len(shapes))

    def get_on_spec(self, attribute_name=None, attribute=None):
        """ Public method, to return a list of outline polygons, based upon the optional selection criteria """
        shapes_selected = self.shapes_gdf

        # optional filter on a certain specified attribute (only 1 attribute supported)
        if attribute_name is not None and attribute is not None:
            shapes_selected = shapes_selected[(shapes_selected[attribute_name] == attribute)]

        geoms = []
        for _, outline in shapes_selected.iterrows():
            geoms.append(outline['geometry'])

        return shapes_selected, geoms

    def _load_shapes_gpd(self):
        """ Private method to load the shapes - using geopandas
            Also some manipulations/corrections on the data could be done here (if needed)
        """

        # Init the shape geo-dataframe
        shapes_gdf = None
        shapes_crs = None

        # get all shape files to load
        shape_files = self._get_shape_files()

        for shape_file in shape_files:
            # Load a shapefile
            tmp_shape_gdf = gpd.read_file(shape_file)

            # if you want to rename fields, or other corrections, do it here
            # also here you can add specific checks if a certain attribute is present

            # Add it to the variable collecting all shapes:
            if shapes_gdf is None:
                shapes_gdf = tmp_shape_gdf
                shapes_crs = shapes_gdf.crs
            else:
                # should we have a check here that the CRS systems are the same before appending?
                if tmp_shape_gdf.crs.equals(shapes_gdf.crs):
                    shapes_gdf = shapes_gdf.append(tmp_shape_gdf)
                else:
                    raise Exception("geo-dataframes being appended do not have the same CRS !!")

        return shapes_gdf, shapes_crs

    def _get_shape_files(self):
        # check that path exists
        if Path(self.geofile).exists():
            return [self.geofile]
        else:
            raise FileExistsError('Cannot find the provided path: {userpath:} from the current directory {curr:}'.format(userpath=self.geofile, curr=Path.cwd()))
