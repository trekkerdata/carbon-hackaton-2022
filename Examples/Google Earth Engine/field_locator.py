from polygon_loader import PolygonLoader
import geopandas as gpd

import matplotlib.pyplot as plt
# import contextily as cx


class FieldLocator:
    def __init__(self, source, attribute_name=None, attribute_list=[]):
        # Source is the path to the shapefile to load
        self.source = source

        self.attribute_name = attribute_name
        # Input checking
        if isinstance(attribute_list, list):
            self.attribute_list = attribute_list
        else:
            raise AttributeError('attribute_list should be specified as a list')

        # get field geometries
        self.field_gdfs, self.field_geoms = self._load_field_geoms()

    def get_field_geoms(self):
        return self.field_geoms

    def plot_field_geoms(self):
        if isinstance(self.field_geoms, list):
            for polygon in self.field_geoms:
                if polygon.geometryType() == 'Polygon':
                    plt.plot(*polygon.exterior.xy)
                elif polygon.geometryType() == 'MultiPolygon':
                    for polygon_part in polygon.geoms:
                        plt.fill(*polygon_part.exterior.xy, 'moccasin')

            plt.title(self.attribute_list)
            plt.show()
        else:
            raise TypeError('Assumption is that self.attribute_list is of type list !')

    # def plot_field_gdfs(self):
    #     # TODO: background now from contextily... but Folium lbirary may better supported
    #     if isinstance(self.field_gdfs, gpd.geodataframe.GeoDataFrame):
    #         ax = self.field_gdfs.plot()
    #         cx.add_basemap(ax, crs=self.field_gdfs.crs)
    #
    #         plt.title(self.field_names)
    #         plt.show()

    def _load_field_geoms(self):
        polygonloader = PolygonLoader(geofile=self.source)
        gdfs = None

        polygons = []
        # For every provided attribute, fetch its polygon(s), and keep appending

        if len(self.attribute_list) > 0:
            for attribute in self.attribute_list:
                gdf_selected, polygon_selected = polygonloader.get_on_spec(attribute_name=self.attribute_name,
                                                                           attribute=attribute)
                # both keep track/stack the gdf as the shapes
                polygons += polygon_selected
                if gdfs is None:
                    gdfs = gdf_selected
                else:
                    gdfs = gdfs.append(gdf_selected)
        else:
            gdf_selected, polygon_selected = polygonloader.get_all()
            # both keep track/stack the gdf as the shapes
            polygons += polygon_selected
            gdfs = gdf_selected

        return gdfs, polygons
