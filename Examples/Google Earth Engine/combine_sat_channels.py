from rasterio import rasterio
import numpy as np
import matplotlib.pyplot as plt


def load_tif(fullfilepath):
    with rasterio.open(fullfilepath) as src:
        np_array = src.read()
        profile = src.profile

    return np_array, profile


def main():
    src_dir = './sat_imgs/'
    field_name = 'field_flevo_2'
    channel = 'RGB'

    src_dir = src_dir + field_name + '/' + channel + '/'

    red = '2020-07-01_2020-08-01.Red_median.tif'
    green = '2020-07-01_2020-08-01.Green_median.tif'
    blue = '2020-07-01_2020-08-01.Blue_median.tif'

    red_array, profile = load_tif(src_dir + red)
    green_array, _ = load_tif(src_dir + green)
    blue_array, _ = load_tif(src_dir + blue)

    red_array = red_array
    green_array = green_array
    blue_array = blue_array

    rgb_array = np.stack((red_array, green_array, blue_array))
    print(rgb_array.shape)
    rgb_array = np.squeeze(rgb_array, axis=1)

    # Remark: not so clear what the best way is to adjust the brightness
    # rgb_array = (rgb_array-np.min(rgb_array))/(np.max(rgb_array)-np.min(rgb_array))
    BRIGHTNESS_FACTOR = 2
    rgb_array = rgb_array * BRIGHTNESS_FACTOR

    profile.update(count=3)
    # profile['photometric'] = "RGB"
    # print(profile)
    print(np.min(rgb_array), np.max(rgb_array))
    plt.imshow(np.moveaxis(rgb_array, 0, 2))
    plt.show()

    # write to disk
    with rasterio.open('./sat_imgs/' + field_name + '.tif', 'w', **profile) as dst:
        #dst.write(rgb_array.astype(rasterio.uint8))
        dst.write(rgb_array)


if __name__ == "__main__":
    main()
